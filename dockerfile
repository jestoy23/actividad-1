#partimos de una base oficial de python
FROM python:3.8-buster
RUN mkdir /directioapp
#El directorio de trabajo es desde donde se ejecuta el contenedor al iniciarse
WORKDIR /directorioapp

#copiamos todos los archivos del build context al directiorio /app del contenedor 
ADD . /directorioapp

#Ejecutamos pip para instalar las dependencias en el contenedor
RUN pip install --trusted-host pypi.python.org -r requirements.txt 
#RUN python manage.py runserver --settings-core.settings-base 

#Exponer puerto 8000
